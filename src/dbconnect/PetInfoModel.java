package dbconnect;

import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;


import dbconnect.PetInfo;
import dbconnect.MyDBConnect;

public class PetInfoModel {
	private List<PetInfo> petinfoList = new ArrayList<PetInfo>();

	public List<PetInfo> getPetinfoList() {
		return petinfoList;
	}

	public void setPetinfoList(List<PetInfo> petinfoList) {
		this.petinfoList = petinfoList;
	}
	
	public List<PetInfo> getPetInfo() throws Exception {
		PetInfo petinfo = null;
		ResultSet rs = null;
		MyDBConnect dbinfo = new MyDBConnect();
		try {
			rs = dbinfo.display();
			if (rs != null) {
				while(rs.next()) {
					petinfo = new PetInfo();
					petinfo.setName(rs.getString("name"));
					petinfo.setOwner(rs.getString("owner"));
					petinfo.setSpecies(rs.getString("species"));
					petinfoList.add(petinfo);
				}
			}
			
			
		} catch (Exception e){
		  e.printStackTrace();
		}
		return this.petinfoList;
	}
}

