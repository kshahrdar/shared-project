package dbconnect;

import java.io.*;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
//import com.fasterxml.jackson.databind.*;
//import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.annotation.JsonMerge;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import dbconnect.MyDBConnect;
import dbconnect.PetInfoModel;
import ognl.ParseException;

/**
 * Servlet implementation class JSONservlet
 */
@WebServlet("/JSONservlet")
public class JSONservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public JSONservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
//    	List<String> list = new ArrayList<String>();
//	    list.add("item1");
//	    list.add("item2");
//	    list.add("item3");
//	    String json = new Gson().toJson(list);

//	    //response.setContentType("application/json");
//	    response.setCharacterEncoding("UTF-8");
//	    //response.getWriter().write(json);
//	    response.getWriter().append("hello");		
		
			
    	//response.getWriter().append("you passed ").append(request.getContextPath());
		
		
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    }
    
	    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		//response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = new BufferedReader(request.getReader());
			while ((line = reader.readLine()) != null)
				jb.append(line);	
			System.out.println(jb);
			String json = jb.toString();
			
			JsonParser parser = new JsonParser();
			JsonElement jsonTree = parser.parse(json);
			
			if(jsonTree.isJsonObject()) {
				JsonObject jsonObj = jsonTree.getAsJsonObject();
				JsonElement f1 = jsonObj.get("key1");
				JsonElement f2 = jsonObj.get("key2");
				System.out.println(f1);
				System.out.println(f2);			
				
					//PrintWriter out = response.getWriter();
					//out.print(jsonResponse);
			}
				

				//Now query
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/myDB?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=America/Chicago", "root", "hippogriff182");
				try {
					String sql = "SELECT * FROM pet";
					PreparedStatement ps = conn.prepareStatement(sql);
					ResultSet rs = ps.executeQuery();		
					
					JSONArray jsonarray = new JSONArray();
					JSONObject jsonobj = new JSONObject();
					ResultSetMetaData metadata = rs.getMetaData();
					int numColumns = metadata.getColumnCount();
					
					while(rs.next())
					{				
						for (int i = 1; i<=numColumns; ++i)
						{String column_name = metadata.getColumnName(i);
						jsonobj.put(column_name,  rs.getObject(column_name));
						}
						jsonarray.put(jsonobj);
					}
					System.out.println(jsonarray.toString());
					String jsonResponse = jsonarray.toString();
					PrintWriter out = response.getWriter();
					out.append(jsonResponse);
					
					
					
				} finally {
					conn.close();
				}
					
			

				} catch (Exception e) {}
		doGet(request, response);
	}
}
		
		

	


