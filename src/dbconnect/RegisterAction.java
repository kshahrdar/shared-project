package dbconnect;

import dbconnect.MyDBConnect;
import com.opensymphony.xwork2.ActionSupport;

public class RegisterAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name, owner, species, msg;
	MyDBConnect dao = null;
	
	public String execute() throws Exception {
		dao = new MyDBConnect();
		msg = dao.register(name, owner, species);
		return "SUCCESS";
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getSpecies() {
		return species;
	}
	
	public void setSpecies(String species) {
		this.species = species;
	}
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg=msg;
	}

}
