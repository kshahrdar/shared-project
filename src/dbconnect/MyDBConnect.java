package dbconnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class MyDBConnect {
	public static Connection conn() throws Exception	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/myDB?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=America/Chicago", "root", "hippogriff182");
	}
		
	
	public String register(String name, String owner, String species) throws SQLException, Exception {
		try {
			String sql = "INSERT INTO pet VALUES (?,?,?)";
			PreparedStatement ps = conn().prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, owner);
			ps.setString(3, species);
			ps.executeUpdate();
			return "Successful Registration";			
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		} finally {
			if(conn()!= null) {
				conn().close();
			}
		}
	}
	
	public ResultSet display() throws SQLException, Exception {
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM pet";
			PreparedStatement ps = conn().prepareStatement(sql);
			rs = ps.executeQuery(sql);
			return rs;			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if(conn() != null) {
				conn().close();
			}
		}
	}
	
}
