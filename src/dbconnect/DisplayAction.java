package dbconnect;

import java.util.List;

import dbconnect.PetInfo;
import dbconnect.PetInfoModel;

import com.opensymphony.xwork2.ActionSupport;



public class DisplayAction extends ActionSupport {
	private static final long serialVersionUID = -3294264776414191341L;
	
	private PetInfoModel petinfomodel = new PetInfoModel();
	private List<PetInfo> petinfoList;
	
	
	public List<PetInfo> getPetinfoList() {
		return petinfoList;
	}
	public void setPetinfoList(List<PetInfo> petinfoList) {
		this.petinfoList = petinfoList;
	}
	
	
	public String execute() throws Exception {
		this.petinfoList = petinfomodel.getPetInfo();
		System.out.println("end of DisplayAction");
		return "DISPLAY";
	}	

}
