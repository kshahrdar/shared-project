<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Display Data</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type = "text/javascript">



</script>

<s:actionerror />
</script>
</head>
<body>
<table border="1">
<thead>
<tbody id="tbody"></tbody>
<tr>
	<th>Pet Name </th>
	<th> Owner Name </th>
	<th> Species</th>
</tr>
</thead>
<s:actionerror />
	<s:iterator value="petinfoList">
		<tr>
			<td><s:property value = "name" /></td>
			<td><s:property value = "owner" /></td>
			<td><s:property value = "species" /></td>
		</tr>
	</s:iterator>	
</table>
<a href="register.jsp">Add New Pet</a>
</body>
</html>