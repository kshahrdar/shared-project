<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register</title>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type = "text/javascript">


function register() {
	var name = $("#name").val();
	var owner = $("#owner").val();
	var species = $("#species").val();
	$.ajax({
		type : "POST",
		url : "registeraction",
		data: "name=" + name + "&owner=" + owner + "&species=" +species,
		success: function (data) {
			$("#reg").html(data);
		},
		error : function (data) {
			alert("Some error occured");
		}
	});
}	

function display() {
		$.ajax({
			type: "GET",
			url: "displayaction",
			success: function(result) {
				var table = "";
				$.each(result.petinfoList, function() {
					table+= "<tr><td>" + this.name + "</td>" + "<td>" + this.owner + "</td>" + "<td>" + this.species + "</td></tr>";
				})
				console.log('blkaj');
				$("#tbody").html(result);
				alert("end of ajax display");
			},
			error: function(result) {
				alert("error");
			}
		});	
}

	</script>

<s:actionerror />
<body>
<s:form>
<button type="button" onclick="display();">Display Data</button>
<div id="tbody"></div>
</s:form>
</body>

<s:actionerror />
	<form> 
		Name<input type = "text" name="name" id = "name"/>
		Owner<input type = "text" name="owner" id = "owner"/>
		Species<input type = "text" name="species" id = "species"/>
		<button type="button" onclick="register();">Register</button>
		<div id ="reg"></div>
	</form>
</body>
</html>